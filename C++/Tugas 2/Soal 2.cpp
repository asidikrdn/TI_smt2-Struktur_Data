// Ahmad Sidik Rudini
// 201106041165

#include<iostream>
using namespace std;

int main(){
	float nilai[] = {77.5, 66.1, 87.3, 76.9, 69.3};
	
	float jumlah = 0;
	int cacah = sizeof(nilai) / sizeof(float);
	for (int i =0; i < cacah; i++)
		jumlah += nilai[i];
		
	float rerata = jumlah / cacah;
	
	cout << "Rerata : " << rerata << endl;
	
	return 0;
}

