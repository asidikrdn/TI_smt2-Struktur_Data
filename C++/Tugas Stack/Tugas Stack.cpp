// Ahmad Sidik Rudini (201106041165)
// Tugas Mata Kuliah Struktur Data
// Materi Stack (LIFO)

#include<iostream>
#define MAX_SIZE 101
using namespace std;

int A[MAX_SIZE];
int top = -1;

// membuat fungsi push
void push(int x){
	if(top == MAX_SIZE-1){
		printf("Error : Stack Penuh");
		return;
	}
	A[++top] = x;
}

// membuat fungsi yang isinya nilai terakhir/terbaru pada stack
int Top(){
	return A[top];
}

// membuat fungsi pop
void pop(){
	if(top == -1){
		printf("Error : Tidak ada elemen untuk di pop");
		return;
	}
	top--;
}

// membuat fungsi print
void print(){
	int i;
	printf("Stack : ");
	for(i=0; i<=top; i++){
		printf("%d ", A[i]);
	}
}

// menjalankan fungsi pop dan push dalam fungsi main
int main(){
	printf("Stack awalnya kosong"); printf("\n");
	print(); printf("\n"); printf("\n"); 
	push(1); push(3); push(5); push(7); push(9); push(2); push(4); push(6); push(8); push(10);
	printf("Stack berisi 10 elemen setelah melakukan push 10 kali"); printf("\n");
	print(); printf("\n"); printf("\n"); 
	printf("Stack berkurang satu persatu karena dijalankan fungsi pop"); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
	pop(); print(); printf("\n");
}
