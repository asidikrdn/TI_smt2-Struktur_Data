//Nama : Ahmad Sidik Rudini (201106041165)
//UTS Struktur DATA

#include <stdio.h>
#include <conio.h>
#include <string.h>

struct 
{
	char npm[20];
	char nama[30];
	int gender;
	char kelamin[20];
	char prodi[30];
	char kelas[30];
} mhs[20];	

int main()
{
	int i, b;
	
	printf("Berapa jumlah mahasiswa yang akan diinput datanya ? ");
	scanf("%d", &b);
	
	for(i=0;i<b;i++)
	{
		printf ("\nMAHASISWA %d \n", i+1);
		printf("Masukkan NPM : ");
		scanf("%s",&mhs[i].npm);
		fflush(stdin);
		printf("Masukkan Nama : ");
		gets(mhs[i].nama);
		printf("Masukkan Jenis Kelamin : \nKet: Tekan sesuai angka dibawah \n1. Laki-laki \n2. Perempuan \n=>");
		scanf("%d",&mhs[i].gender);
		fflush(stdin);
		printf("Masukkan Program Studi : ");
		gets(mhs[i].prodi);
		printf("Masukkan Kelas : ");
		gets(mhs[i].kelas);
		
		if (mhs[i].gender == 1){
			strcpy (mhs[i].kelamin ,"Laki-laki");
		}
		else if (mhs[i].gender == 2){
			strcpy (mhs[i].kelamin ,"Perempuan");
		}
		else {
			strcpy (mhs[i].kelamin ,"Input Salah"); 
		}
		
		printf("\n\n");
	}
	
	printf("\n\n----------------------------------------------------------------------------");
	printf ("\nDATA MAHASISWA");
	printf("\n----------------------------------------------------------------------------");
	for(i=0;i<b;i++)
	{
		printf ("\nMAHASISWA %d \n", i+1);
		printf ("Nama \t\t:");
		printf ("%s", mhs[i].nama );
		printf ("\nNPM \t\t:");
		printf ("%s", mhs[i].npm  );
		printf ("\nJenis Kelamin \t:");
		printf ("%s", mhs[i].kelamin  );
		printf ("\nProgram Studi \t:");
		printf ("%s", mhs[i].prodi  );
		printf ("\nKelas \t\t:");
		printf ("%s", mhs[i].kelas  );
		
		printf("\n\n");
	}
	getch();
}
