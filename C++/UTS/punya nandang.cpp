#include <stdio.h>
#include <conio.h>

struct 
{
	char id[20];
	char nama[30];
	char gender[10];
	char no_hp[13];
} mahasiswa[10];	

int main()
{
	int i, n;
	
	printf("Berapa Mahasiswa yang akan diinput ? ");
	scanf("%d", &n);
	
	for(i=0;i<n;i++)
	{
		printf ("\nData ke- %d \n", i+1);
		printf("Masukkan Nomor Induk Mahasiswa : ");
		scanf("%s",&mahasiswa[i].id );
		printf("Masukkan Nama : ");
		scanf("%s",&mahasiswa[i].nama );
		printf("Masukkan Jenis Kelamin (Pria/Wanita) : ");
		scanf("%s",&mahasiswa[i].gender);
		printf("Masukkan Nomor Handphone : ");
		scanf("%s",&mahasiswa[i].no_hp);
		printf("\n\n");
	}
	
	printf ("\nDATA MAHASISWA");
	for(i=0;i<n;i++)
	{
		printf ("\nDATA ke- %d \n", i+1);
		printf ("Nama \t\t:");
		printf ("%s", mahasiswa[i].nama );
		printf ("\nNomor Induk \t:");
		printf ("%s", mahasiswa[i].id  );
		printf ("\nJenis Kelamin \t:");
		printf ("%s", mahasiswa[i].gender  );
		printf ("\nNomor HP \t:");
		printf ("%s", mahasiswa[i].no_hp  );
		
		printf("\n\n");
	}
	getch();
}
